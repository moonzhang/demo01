package lambda;

import javax.swing.*;
import java.util.Arrays;
import java.util.Date;

public class LambdaTest {
    public static void main(String[] args) {
        //泛型
        String[] plants = new String[]{"nina","moon","sky","jokbs","billgoods"};
        System.out.println(Arrays.toString(plants));
        Arrays.sort(plants);
        System.out.println("排序后的数组：");
        System.out.println(Arrays.toString(plants));
        Arrays.sort(plants,(first,send)->first.length()-send.length());
        System.out.println("lambda序后的数组：");
        System.out.println(Arrays.toString(plants));
        Timer t = new Timer(1000, event-> System.out.println("this"+new Date()));

            t.start();
            JOptionPane.showMessageDialog(null,"关闭把");
            System.exit(0);
    }
}
