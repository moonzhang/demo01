package lambda;

import java.io.*;
import java.time.LocalDate;

public class Employee {
    private String name;
    private double salary;
    private LocalDate hireDay;

    public Employee(String name, double salary, LocalDate hireDay) {
        this.name = name;
        this.salary = salary;
        this.hireDay = hireDay;
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public LocalDate getHireDay() {
        return hireDay;
    }

    public static void main(String[] args) throws IOException {
         Employee employee = new Employee("張三",5000,  LocalDate.of(2019,5,2));
        File  file = new File ("D:/a.txt");
        FileOutputStream outputStream = new FileOutputStream("d:/A.TXT");
        outputStream.write(employee.getName().getBytes());
        outputStream.close();


         System.out.println( employee.getName());


        System.out.println(employee.getSalary());
    }
}
