package equals;

import java.time.LocalDate;
import java.util.Objects;

public class Employee {
    private String name;
    private double salry;
    private LocalDate date;

    public Employee(String name,double salry ,int year ,int moth,int day){
        this.name = name;
        this.salry = salry;
        date = LocalDate.of(year,moth,day); 

    }

    public String getName() {
        return name;
    }

    public void  raiseSalry( double byPercent) {
        double raise = salry*byPercent/100;
        this.salry=+raise;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Double.compare(employee.salry, salry) == 0 &&
                Objects.equals(name, employee.name) &&
                Objects.equals(date, employee.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, salry, date);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", salry=" + salry +
                ", date=" + date +
                '}';
    }
}
