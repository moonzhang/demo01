package abstractClasses;

import java.time.LocalDate;

public class Employee extends  Person {
    private double salary;
    private LocalDate date;

    @Override
    public String getDescription() {
        return String.format("an employee with a salary of $%,2f",salary);
    }
    public  Employee(String name ,double salary,int year,int month ,int dat){
        super(name);
        this.salary = salary;
        this.date = LocalDate.of(year,month,dat);
    }
    public  double getSalary(){
        return  this.salary;
    }

    public LocalDate getDate() {
        return date;
    }
    public  void  raiseSalary(double byPercent){
        double raise = salary*byPercent/100;
        salary+=raise;
    }

}
