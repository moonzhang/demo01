package abstractClasses;

public class TestAbstract {
    public static void main(String[] args) {
        Person person[] = new Person[2];
        person[0] = new Employee("moon" ,5000,2018,5,9);
        person[1] = new Student("明晓溪","english");
        for (Person p:person) {
            System.out.println(p.getName()+","+p.getDescription());

        }

    }
}
