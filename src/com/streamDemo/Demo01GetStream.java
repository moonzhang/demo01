package com.streamDemo;

import java.util.*;
import java.util.stream.Stream;

public class Demo01GetStream {
    public static void main(String[] args) {
        List<String > arrayStream = new ArrayList<>();
        Stream<String> stream1 = arrayStream.stream();
        Set<String>  stringSet = new HashSet<>();
        Stream<String> stream = stringSet.stream();
        Map<String ,Integer>  map = new HashMap<>();
        Set<String> stringSet1 = map.keySet();
        Stream<String> stream2 = stringSet1.stream();
        Collection<Integer> values = map.values();
        Stream<Integer> stream3 = values.stream();
        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        Stream<Map.Entry<String, Integer>> stream4 = entries.stream();
        Stream<String> stream5 = Stream.of("张三", "历史", "王五", "糟溜", "天气");
        stream5.forEach(name-> System.out.println(name));


    }
}
