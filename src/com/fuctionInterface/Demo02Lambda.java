package com.fuctionInterface;
/*
使用lambda来优化
 */
public class Demo02Lambda {
    public static void  showInfo(int level ,BuilderLogger builderLogger){
        if (level==1 ){
            System.out.println( builderLogger.builderInfo());
        }
    }

    public static void main(String[] args) {
        String mas1 = "Hello";
        String masg2 ="moon";
        String masg3 ="nihao";
        showInfo(1,(()->{return mas1+masg2+masg3;}));
    }

}
