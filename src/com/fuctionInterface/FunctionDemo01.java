package com.fuctionInterface;

@FunctionalInterface
public interface FunctionDemo01  {
    abstract void show();
}
class  FumctionDemo01Impl implements  FunctionDemo01{

    @Override
    public void show() {
        System.out.println("显示实现impl的方法");
    }
}

class  Demo01Test{
    public static  void shows(FunctionDemo01 mydemo){
        mydemo.show();
    }
    public static void main(String[] args) {
        FunctionDemo01 functionDemo01 = new FumctionDemo01Impl();
        functionDemo01.show();
        FunctionDemo01 functionDemo011 = new FunctionDemo01() {
            @Override
            public void show() {
                System.out.println("匿名内部类实现");
            }
        };

        shows(()->{
            System.out.println("lambda表达式");
        });
        //简化
        shows(()-> System.out.println("简化的lambda表达式"));


    }
}
