public class GenericDemo<T> {
    private T first;
    private T second;

    public GenericDemo() {
        first = null;
        second = null;
    }

    public GenericDemo(T first) {
        this.first = first;
    }

    public GenericDemo(T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public T getSecond() {
        return second;
    }

    public void setSecond(T second) {
        this.second = second;
    }
}
